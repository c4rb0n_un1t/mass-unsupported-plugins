
TARGET = DayPlan
TEMPLATE = lib
QT += widgets



include(../../Common/common.pri)

include(../../plugin.pri)

include(../../Interfaces/Architecture/PluginBase/PluginBase.pri)

SOURCES += \
    dayplan.cpp

HEADERS += \
    dayplan.h

DISTFILES += \
    PluginMeta.json
